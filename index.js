module.exports = {

    substract: (num1, num2) => num1 - num2,

    add: (num1, num2) => num1 + num2,

    multiply: (num1, num2) => num1 * num2

}