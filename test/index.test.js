const expect = require('chai').expect,
      { substract, add, multiply } = require('../index')

describe('index.js tests', () => {

    describe('math.substract() Test', () => {

        it('should equal 4', () => expect(substract(5, 1)).to.equal(4))
        it('should equal 2', () => expect(substract(4, 2)).to.equal(2))

    })

    describe('math.add() Test', () => {

        it('should equal 2', () => expect(add(1, 1)).to.equal(2))
        it('should equal 4', () => expect(add(2, 2)).to.equal(4))

    })
    
    describe('math.multiply() Test', () => {

        it('should equal 3', () => expect(multiply(3, 1)).to.equal(3))
        it('should equal 10', () => expect(multiply(5, 2)).to.equal(10))

    })

})